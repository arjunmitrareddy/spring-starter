package com.boot.controller;

import com.boot.model.Shipwreck;
import com.boot.repository.ShipWreckRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * Created by arjunMitraReddy on 5/24/2017.
 */
@RestController
@RequestMapping("api/v1/")
public class ShipWreckController {

    @Autowired
    private ShipWreckRepository shipWreckRepository;

   @RequestMapping(value="shipwrecks", method=RequestMethod.GET)
    public List<Shipwreck> list() {
        return shipWreckRepository.findAll();
    }

    @RequestMapping(value="shipwrecks", method=RequestMethod.POST)
    public Shipwreck create(@RequestBody Shipwreck shipwreck) {
        return shipWreckRepository.saveAndFlush(shipwreck);
    }

    @RequestMapping(value="shipwrecks/{id}", method=RequestMethod.GET)
    public Shipwreck get(@PathVariable Long id) {
        return shipWreckRepository.findOne(id);
    }

    @RequestMapping(value="shipwrecks/{id}", method=RequestMethod.PUT)
    public Shipwreck update(@PathVariable Long id, @RequestBody Shipwreck shipwreck) {
       Shipwreck existingShipWreck = shipWreckRepository.findOne(id);
        BeanUtils.copyProperties(shipwreck, existingShipWreck);
        return shipWreckRepository.saveAndFlush(existingShipWreck);
    }

    @RequestMapping(value="shipwrecks/{id}", method=RequestMethod.DELETE)
    public Shipwreck delete(@PathVariable Long id) {
       Shipwreck existingShipWreck = shipWreckRepository.findOne(id);
       shipWreckRepository.delete(existingShipWreck);
       return existingShipWreck;
    }

}
