package com.boot.repository;

import com.boot.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by arjunMitraReddy on 5/25/2017.
 */

public interface ShipWreckRepository extends JpaRepository<Shipwreck, Long>{

}
