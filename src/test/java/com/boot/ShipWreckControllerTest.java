package com.boot;

/**
 * Created by arjunMitraReddy on 5/25/2017.
 */
import com.boot.controller.ShipWreckController;
import com.boot.model.Shipwreck;
import com.boot.repository.ShipWreckRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;


public class ShipWreckControllerTest {

    @InjectMocks
    private ShipWreckController sc;

    @Mock
    private ShipWreckRepository shipWreckRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testShipWreckGet() {
        Shipwreck sw = new Shipwreck();
        sw.setId(1L);

        when(shipWreckRepository.findOne(1L)).thenReturn(sw);
        Shipwreck shipwreck = sc.get(1L);

        verify(shipWreckRepository).findOne(1L);

        //assertEquals(1L, shipwreck.getId().longValue());
        assertThat(shipwreck.getId(), is(1L));
    }
}
