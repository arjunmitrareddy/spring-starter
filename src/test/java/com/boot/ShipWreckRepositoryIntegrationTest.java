package com.boot;

import com.boot.model.Shipwreck;
import com.boot.repository.ShipWreckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.List;

/**
 * Created by arjunMitraReddy on 5/25/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(App.class)
public class ShipWreckRepositoryIntegrationTest {

    @Autowired
    ShipWreckRepository shipWreckRepository;

    @Test
    public void testFindAll() {
        List<Shipwreck> shipwrecks = shipWreckRepository.findAll();
        assertThat(shipwrecks.size(), is(greaterThanOrEqualTo(0)));

    }
}
